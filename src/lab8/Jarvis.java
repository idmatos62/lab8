/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lab8;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

/**
 *
 * @authors Ashley Huskey and Ismael D. Matos
 * @courseNumber 44-242
 * @assignmentName Lab 8
 * @since October 24, 2013
 */
public class Jarvis {

    static ArrayList<String> dictionary = new ArrayList<>();

    public static void readDictionary(String nameOfFile) throws FileNotFoundException {
        File file = new File(nameOfFile);
        Scanner input = new Scanner(file);
        while (input.hasNext()) {
            dictionary.add(input.nextLine());
        }
    }
    
    public static boolean checkDictionaryFor(String word) {
        return dictionary.contains(word);
    }
    // Ashley created centurion
    public static String centurion(char ch, ArrayList<String> history) {
        ArrayList<String> tempDic = new ArrayList<>(dictionary);
        tempDic.removeAll(history);
        for (String word : tempDic) {
            if (word.length() == 3 && word.charAt(0) == ch) {
                return word;
            }
        }
        return null;
    }
    //Ashley created countdown
    public static String countdown(ArrayList<Character> usables) {
        String value;
        for (String word : dictionary) {
            value = word;
            for (char ch : word.toCharArray()) {
                if (!usables.contains(ch)) {
                    value = null;
                }
            }
            if (value != null) {
                return value;
            }
        }
        return null;
    }

    public static String playAcrostics(String word, int position) {
        for (String testWord : dictionary) {
            if (testWord.charAt(0) == word.charAt(position)) {
                if (testWord.charAt(testWord.length() - 1) == word.charAt(word.length() - (1 + position))) {
                    return testWord;
                }
            }
        }
        return "[no such word]";
    }

    public static int playBeheadments() {
        int score = 0;
        System.out.println("Player 2: ");
        for (int i = 0; i < 100; i++) {
            Random generator = new Random();
            int randomIndex = generator.nextInt(58112);
            String testWord = (String) dictionary.get(randomIndex);
            if (Jarvis.checkDictionaryFor(testWord.substring(1))) {
                System.out.print(testWord + " ");
                score++;
            }
        }
        if (score == 0) {
            System.out.println("Jarvis couldn't think of any!");
        }
        return score;
    }

    public static char playGhosts(String incompleteWord) {
        for (int i = 97; i < 123; i++) {
            incompleteWord += (char) i;
            for (String testing : dictionary) {
                if (testing.startsWith(incompleteWord) && incompleteWord.length() < 3) {
                    System.out.println("Player 2: " + incompleteWord);
                    return (char) i;
                } else if (testing.startsWith(incompleteWord) || incompleteWord.equals(testing)) {
                    System.out.println("Player 2: " + incompleteWord);
                    return (char) i;
                }
            }
            incompleteWord = incompleteWord.substring(0, incompleteWord.length() - 1);
        }
        System.out.println("Nice try, but that's not a recognized word!");
        return '%';
    }
}
