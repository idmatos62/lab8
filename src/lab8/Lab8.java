package lab8;

import java.io.FileNotFoundException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @authors Ashley Huskey and Ismael D. Matos
 * @courseNumber 44-242
 * @assignmentName Lab 8
 * @since October 24, 2013
 */
public class Lab8 {

    public static void main(String[] args) throws FileNotFoundException {
        //This statement is important, it points to the dictionary 
        //file Jarvis will use
        Jarvis.readDictionary("corncob_lowercase.txt");
        menu();
    }
    //Ashley created the menu
    //Menu allows the user to choice what game to play
    public static void menu() {
        int choice = -1;
        while (choice != 5) {
            System.out.print("Pick a game, any game:\n"
                    + "0. Centurion\n"
                    + "1. Countdown\n"
                    + "2. Acrostics\n"
                    + "3. Beheadments\n"
                    + "4. Ghosts\n"
                    + "5. Quit\n\n"
                    + "Choice: ");
            //gets the users choice
            choice = new Scanner(System.in).nextInt();
            //uses the choice to start the game the user picks
            switch (choice) {
                case 0:
                    centurion();
                    break;
                case 1:
                    countdown();
                    break;
                case 2:
                    acrostics();
                    break;
                case 3:
                    beheadments();
                    break;
                case 4:
                    ghosts();
                    break;

            }
        }
    }
    // Ashley created centurion
    //The letters of the alphabet are given scores, so that a=1, b=2, etc. 
    //The first player writes down a three-letter word and, beside it, 
    //the total score made by its letters. 
    //The second player writes another 3 letter word underneath it, 
    //starting with the last letter of the preceding word, 
    //and writing its score against it. 
    //No word may be repeated, and the words must be found in a dictionary. 
    //[If the human player supplies a word that is not in the dictionary, 
    //they automatically lose the game.]  
    //The scores accumulate as the game progresses, 
    //and the loser is the first player who makes the score total 100 or more.
    private static void centurion() {
        Scanner input = new Scanner(System.in);
        int playerScore = 0;
        int jarvisScore = 0;
        int numTurn = 0;
        ArrayList<String> history = new ArrayList<>();
        //makes sure that the scores are not 100 or more 
        //which would cause one of them to win.
        while (playerScore < 100 && jarvisScore < 100) {
            String playerWord;
            System.out.print("Player 1: ");
            playerWord = input.nextLine();
            //makes sure the word is of length 3
            if (playerWord.length() != 3) {
                System.out.println("Your word's length was not 3."
                        + "\nPlayer 2 wins!");
                return;
            }
            //Makes sure this is not the first turn (exception to this code)
            if (numTurn != 0) {
                String previousWord = history.get(history.size() - 1);
                char c = previousWord.charAt(2);
                if (c != playerWord.charAt(0)) {
                    System.out.println("Your word didn't start with the last "
                            + "letter of the previous word.\nPlayer 2 wins!");
                    return;
                }
            }
            //checks to see if the player's word is in the dictionary
            if (!Jarvis.checkDictionaryFor(playerWord)) {
                System.out.println("Your word was not in the dictionary."
                        + "\nPlayer 2 wins!");
                return;
            }
            //checks to see if the player's word has been used already
            if (history.contains(playerWord)) {
                System.out.println("Your word has already been used."
                        + "\nPlayer 2 wins!");
                return;
            }
            //calculates the player's score
            for (char cha : playerWord.toCharArray()) {
                playerScore += cha - 'a' + 1;
            }
            System.out.println("Player 1 score: " + playerScore);
            //determines if player lost
            if (playerScore >= 100) {
                System.out.println("Player 2 wins!");
                return;
            }
            history.add(playerWord);
            //get a word from jarvis that has not been used already
            String jarvisWord = Jarvis.centurion(playerWord.charAt(2), history);
            System.out.println("Player 2: " + jarvisWord);
            //calculates jarvis' score
            for (char cha : jarvisWord.toCharArray()) {
                jarvisScore += cha - 'a' + 1;
            }
            System.out.println("Player 2 score: " + jarvisScore);
            //checks to see if jarvis lost
            if (jarvisScore >= 100) {
                System.out.println("Player 1 wins!");
                return;
            }
            history.add(jarvisWord);
        }
    }
    //Ashley created countdown
    //Nine distinct letters are chosen at random
    //and players have to make the longest word they can 
    //from those letters within a set time-limit (say, 30 seconds).
    private static void countdown() {
        Random rng = new Random();
        Scanner input = new Scanner(System.in);
        ArrayList<Character> characters = new ArrayList<>(9);
        while (characters.size() != 9) {
            char rand = (char) (rng.nextInt(26) + 97);
            //checks to see if rand is already in characters, 
            //if not then it adds it
            if (!characters.contains(rand)) {
                characters.add(rand);
            }
        }
        System.out.print("Letters: ");
        //get letters from characters and prints them out with a - between them
        for (int index = 0; index < characters.size(); index++) {
            System.out.print(characters.get(index));
            if (index != (characters.size() - 1)) {
                System.out.print("-");
            }
        }
        System.out.println();
        System.out.print("You have 30 seconds to enter the longest possible "
                + "word you can think of comprised of those letters."
                + "\nPlayer 1: ");
        long startTime = System.currentTimeMillis();
        String playerAnswer = input.nextLine();
        //checks to see if they went over 30 seconds
        if (System.currentTimeMillis() - startTime > 30000) {
            System.out.println("You ran out of time.\nPlayer 2 wins!");
            return;
        }
        //Check to see if the word is valid or not
        for (char ch : playerAnswer.toCharArray()) {
            if (!characters.contains(ch)) {
                System.out.println("Your word is invalid.\nPlayer 2 wins!");
                return;
            }
        }
        String jarvisAnswer = Jarvis.countdown(characters);
        System.out.println("Player 2: " + jarvisAnswer);
        //checks to see whose word is longer. 
        //The one with the longest word wins.
        if (jarvisAnswer.length() > playerAnswer.length()) {
            System.out.println("Player 2 wins!");
        } else if (playerAnswer.length() > jarvisAnswer.length()) {
            System.out.println("Player 1 wins!");
        } else {
            System.out.println("Tie!");
        }
    }
    
    //Ismael D. created acrostics
    private static void acrostics() {
        System.out.println("Please enter a word to generate acrostics: ");
        Scanner input = new Scanner(System.in);
        String userWord = input.next().toLowerCase();
        int userWordLength = userWord.length();
        int userScore = 0;
        int jarvisScore = 0;
        //This method checks if the user took longer than the time limit after
        //they enter all words
        BigInteger nanoTimeLimit = new BigInteger("300000000000");
        for (int i = 0; i < userWordLength; i++) {
            System.out.print(userWord.charAt(i) + " ----- " + userWord.
                    charAt(userWordLength - (i + 1)));
            System.out.println();
        }
        System.out.println(
                "You have 5 minutes to enter the longest possible words that "
                + "start/end with the indicated letters. "
                + "Enter all words on one line, separated by spaces.");
        long startTime = System.nanoTime();
        System.out.print("Player 1: ");
        for (int i = 0; i < userWordLength; i++) {
            String userAnswer = input.next().toLowerCase();
            if (!Jarvis.checkDictionaryFor(userAnswer)) {
                userScore += 0;
            } else if ((userAnswer.charAt(0) == userWord.charAt(i))
                    && (userAnswer.charAt(userAnswer.length() - 1)
                    == userWord.charAt(userWordLength - (i + 1)))) {
                userScore += userAnswer.length();
            }
        }
        long endTime = System.nanoTime();
        if ((endTime - startTime) > nanoTimeLimit.doubleValue()) {
            System.out.println("Sorry, you took too long!");
            return;
        }
        System.out.println("Player 1 score: " + userScore);
        System.out.print("Player 2: ");
        for (int i = 0; i < userWordLength; i++) {
            String jarvisAnswer = Jarvis.playAcrostics(userWord, i);
            System.out.print(jarvisAnswer + " ");
            if ((jarvisAnswer.charAt(0) == userWord.charAt(i))
                    && (jarvisAnswer.charAt(jarvisAnswer.length() - 1)
                    == userWord.charAt(userWordLength - (i + 1)))) {
                jarvisScore += jarvisAnswer.length();
            }
        }
        System.out.println("\nPlayer 2 score: " + jarvisScore);
        if (userScore > jarvisScore) {
            System.out.println("Player 1 wins!");
        } else if (jarvisScore > userScore) {
            System.out.println("Player 2 wins!");
        } else {
            System.out.println("It's a tie!");
        }

    }

    //Ismael D. created beheadments
    //Jarvis plays by picking 100 random words from the dictionary and checking 
    //if they can be beheaded
    private static void beheadments() {
        System.out.println(
                "You have one minute to write down as many words as they can "
                + "which can be \"beheaded\" to form new words "
                + "(e.g., \"place\" and \"lace\").");
        System.out.println("Player 1: ");
        int userScore = 0;
        Scanner input = new Scanner(System.in);
        //This method checks if the user has exceeded the time limit 
        //using a do-while loop
        long startTime = System.nanoTime();
        BigInteger nanoTimeLimit = new BigInteger("60000000000");
        long endTime;
        do {
            String userWord = input.next();
            endTime = System.nanoTime();
            if (endTime - startTime < nanoTimeLimit.doubleValue()) {
                if (Jarvis.checkDictionaryFor(userWord) && Jarvis.
                        checkDictionaryFor(userWord.substring(1))) {
                    userScore++;
                }
            }
        } while (endTime - startTime < nanoTimeLimit.doubleValue());
        System.out.println("Player 1 score: " + userScore);
        int jarvisScore = Jarvis.playBeheadments();
        System.out.println("\nPlayer 2 score: " + jarvisScore);
        if (userScore > jarvisScore) {
            System.out.println("Player 1 wins!");
        } else if (jarvisScore > userScore) {
            System.out.println("Player 2 wins!");
        } else {
            System.out.println("It's a tie!");
        }
    }

    //Ismael D. created ghosts
    private static void ghosts() {
        System.out.println("The first player thinks of a word of three or more "
                + "letters, and calls out its first letter. "
                + "The second player adds another letter which continues "
                + "but does not complete a word, and so on, "
                + "until one player is forced to finish a word.");
        Scanner input = new Scanner(System.in);
        String word = "";
        int turnTracker = 0;
        while (!Jarvis.checkDictionaryFor(word) || (word.length() < 3)) {
            if ((turnTracker % 2 == 0) || turnTracker == 0) {
                System.out.print("Player 1: ");
                char inputChar = input.next().charAt(turnTracker);
                word += String.valueOf(inputChar);
                turnTracker++;
            } else {
                word += String.valueOf(Jarvis.playGhosts(word));
                turnTracker++;
            }
        }
        if (turnTracker % 2 == 0) {
            System.out.println("Player 1 wins!");
        } else {
            System.out.println("Player 2 wins!");
        }

    }
}
